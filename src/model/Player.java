package model;

public class Player {
    private int x;
    private int y;

    public Player(int x, int y) {
        this.x = x;
        this.y = y;
    }

/*    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }*/

    // Alternative move Methode mit Fensterrand
    public void move(int dx, int dy) {
        if (this.x + dx < Model.WIDTH - getW() / 2 && this.x + dx > 0 + getW() / 2 && this.y + dy < Model.HEIGHT - getH() / 2 && this.y + dy > 0 + getH() / 2) {
            this.x += dx;
            this.y += dy;
        }
    }

    public void moveTo(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getW() {
        return 71;
    }

    public int getH() {
        return 100;
    }
}
