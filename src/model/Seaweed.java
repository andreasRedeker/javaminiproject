package model;

public class Seaweed {
    private int x;
    private int y;
    private float speedY;

    public Seaweed(int x, int y, float speedY) {
        this.x = x;
        this.y = y;
        this.speedY = speedY;
    }

    public void update(long elapsedTime) {
        this.x = Math.round(this.x + elapsedTime * this.speedY);

        if(this.y > Model.HEIGHT && this.speedY > 0   ||  this.y < 0 && this.speedY < 0 ) {
            // this.speedX = -1 * this.speedX;
        }
    }

    public void moveTo(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return 40;
    }

    public int getW() {
        return 30;
    }
}