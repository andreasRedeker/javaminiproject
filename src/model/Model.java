package model;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Model {
    public static final int WIDTH = 600;
    public static final int HEIGHT = 1000;
    private long newWood = 1000; // in Millisekunden
    private long newSeaweed = 1000;
    private int score;
    private int highScore = 0;
    private int baumGenerator = 1;
    public static boolean gameOver = false;

    private Player player;

    public Player getPlayer() {
        return player;
    }

    private List<Wood> woods = new LinkedList<>();
    public List<Wood> getWoods() {
        return woods;
    }

    public List<Seaweed> seaweeds = new LinkedList<>();
    public List<Seaweed> getSeaweeds() {
        return seaweeds;
    }

    private void createSeaweed() {
        int randomY = ThreadLocalRandom.current().nextInt(50, 550);
        int randomInt = ThreadLocalRandom.current().nextInt(7, 12);
        float randomSpeed = randomInt / 100.0f;
        this.seaweeds.add(new Seaweed(-50, randomY, randomSpeed)
        );
    }

    private void createWood() {
        for (int i = 0; i < baumGenerator; i++) {
            int randomY = ThreadLocalRandom.current().nextInt(35, 565);
            int randomInt = ThreadLocalRandom.current().nextInt(7, 12);
            float randomSpeed = randomInt / 100.0f;
            this.woods.add(new Wood(-300, randomY, randomSpeed)
            );
        }
    }

    public Model() {
            for (int i = 0; i < 2; i++) {
                int randomY = ThreadLocalRandom.current().nextInt(35, 565);
                int randomInt = ThreadLocalRandom.current().nextInt(7, 12);
                float randomSpeed = randomInt / 100.0f;
                this.woods.add(new Wood(-200, randomY, randomSpeed)
                );
        }
        this.player = new Player(300, 800);
    }

    public void update(long elapsedTime) {

        for (Seaweed seaweed : seaweeds) {
            seaweed.update(elapsedTime);
        }

        for (Seaweed seaweed : seaweeds) {
            int dx = Math.abs(player.getX() - seaweed.getY());
            int dy = Math.abs(player.getY() - seaweed.getX());
            int w = player.getW() / 2 + seaweed.getW() / 2;
            int h = player.getH() / 2 + seaweed.getH() / 2;

            if (dx <= w  && dy <= h) {
                seaweed.moveTo(-50, -50);
                score++;
            }
        }

        for (Wood wood : woods) {
            wood.update(elapsedTime);
        }

        for (Wood wood : woods) {
            int dx = Math.abs(player.getX() - wood.getY());
            int dy = Math.abs(player.getY() - wood.getX());
            int w = player.getW() / 2 + wood.getW() / 2;
            int h = player.getH() / 2 + wood.getH() / 2;

            if (dx <= w  && dy <= h) {
                score = 0;
                woods.clear();
                seaweeds.clear();
                baumGenerator = 1;
                gameOver = true;
            }
        }

        // Level
        if (score == 10) {
            baumGenerator = 2;
        }
        else if (score == 20) {
            baumGenerator = 3;
        }
        else if (score == 30) {
            baumGenerator = 4;
        }

        // Highscore
        if (score >= highScore) {
            highScore = score;
        }

        if (!gameOver) {
            player.move(0, 1);

            newWood -= elapsedTime;
            if (newWood <= 0) {
                createWood();
                newWood = 3000;
            }

            newSeaweed -= elapsedTime;
            if (newSeaweed <= 0) {
                createSeaweed();
                newSeaweed = 1800;
            }
        }
    }

    public int getScore() {
        return score;
    }

    public int getHighScore() {
        return highScore;
    }
}