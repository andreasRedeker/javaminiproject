import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.Seaweed;
import model.Wood;
import model.Model;
import model.Player;

public class Graphics {
    private Model model;
    private GraphicsContext gc;

    Image wood1 = new Image("res/wood.png");
    Image turtle = new Image("res/turtle.png");
    Image seaweed1 = new Image("res/seaweed.png");

    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    public void draw() {
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

        for (Seaweed seaweed : this.model.getSeaweeds()) {
            gc.drawImage(seaweed1,seaweed.getY() - seaweed.getW() / 2,seaweed.getX() - seaweed.getH() / 2, seaweed.getW(),seaweed.getH()
            );
        }

        for (Wood wood : this.model.getWoods()) {
            gc.drawImage(wood1,wood.getY() - wood.getW() / 2,wood.getX() - wood.getH() / 2, wood.getW(),wood.getH()
            );
        }

        Player p = model.getPlayer();
        gc.drawImage(turtle, p.getX() - p.getW() / 2, p.getY() - p.getH() / 2, p.getW(), p.getH());

        gc.setFont(new Font(20));
        gc.setFill(Color.WHITE);
        gc.fillText("Score: " + model.getScore(), 50,920);

        gc.setFont(new Font(20));
        gc.setFill(Color.WHITE);
        gc.fillText("Highscore: " + model.getHighScore(), 50,950);

        if (model.getScore() == 10) {
            levelUpText();
        }
        else if (model.getScore() == 20) {
            levelUpText();
        }
        else if (model.getScore() == 300) {
            levelUpText();
        }

        if (model.gameOver == true) {
            gc.setFont(new Font(50));
            gc.setFill(Color.WHITE);
            gc.fillText("Game Over!", 175,500);
            gc.setFont(new Font(20));
            gc.setFill(Color.WHITE);
            gc.fillText("press ESC to restart", 225,550);
        }

    }

    private void levelUpText(){
        gc.setFont(new Font(50));
        gc.setFill(Color.WHITE);
        gc.fillText("Level Up!", 200,500);
    }

}
