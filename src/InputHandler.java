import javafx.scene.input.KeyCode;
import model.Model;

public class InputHandler {
    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode key) {
        if (key == KeyCode.UP) {
            model.getPlayer().move(0, -10);
        }
        else if (key == KeyCode.DOWN) {
            model.getPlayer().move(0, 10);
        }
        else if (key == KeyCode.LEFT) {
            model.getPlayer().move(-10, 0);
        }
        else if (key == KeyCode.RIGHT) {
            model.getPlayer().move(10, 0);
        }
        else if (key == KeyCode.ESCAPE) {
            model.getPlayer().moveTo(300,800);
            model.gameOver = false;
        }
    }

    public void onKeyReleased(KeyCode key) {

    }
}
